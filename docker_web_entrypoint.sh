#!/bin/bash

#Install rpy2. #We install this in the docker_web_entrypoint.sh because it needs to be installed after the installation of python and R
pip install rpy2==2.9.2

#configure_rstudio_server
rstudio-server stop
rstudio-server start
userdel rstudio
adduser --disabled-password --gecos "" rstudio
echo rstudio:rstudio | chpasswd

function install_CGAL() {
  apt-get install cmake #To install CGAL
  wget https://github.com/CGAL/cgal/releases/download/releases%2FCGAL-4.13.1/CGAL-4.13.1.tar.xz
  tar -xvf CGAL-4.13.1.tar.xz
  cd CGAL-4.14
  cmake .
  make
  ln -s ./lib/libCGAL.so /usr/lib/x86_64-linux-gnu/libCGAL.so.13
  ln -s ./lib/libCGAL.so /usr/lib/x86_64-linux-gnu/libCGAL.so.13.0.1

  #Create symbolic links to the libCGAL.so.13 library
  ln -s ./CGAL-4.13.1/lib/libCGAL.so /usr/lib/x86_64-linux-gnu/libCGAL.so.13
  ln -s ./CGAL-4.13.1/lib/libCGAL.so /usr/lib/x86_64-linux-gnu/libCGAL.so.13.0.1
}

function install_xml2() {
  ##### xml2 package install fails (libicui18n.so.58 is not found):
  # Download the source code
  wget http://archive.ubuntu.com/ubuntu/pool/main/i/icu/icu_4.8.1.1.orig.tar.gz
  # Extracting the tarball
  tar xzf icu_4.8.1.1.orig.tar.gz
  rm -rf icu_4.8.1.1.orig.tar.gz
  cd icu/source
  # configure and compile
  ./configure
  make
  # Install
  make install
  # Clean up
  rm -rf icu/
  cd ..

  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/icu/source/lib
}

install_CGAL
install_xml2
Xvfb :7 -screen 0 1280x1024x24 #To support a virtual screen environment
