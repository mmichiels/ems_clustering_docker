FROM python:3.6.4-stretch
#The base OS of this container is Debian stretch

RUN apt-get update && \
    apt-get install -y && \
    apt-get install -y netcat software-properties-common gdebi-core gcc g++ cmake make && \
    apt-get install -y libeigen3-dev && \
    apt-get install -y python-pygraphviz graphviz libgraphviz-dev pkg-config python-dev libxml2-dev libxslt-dev unzip libudunits2-dev mpich

RUN mkdir /workspace


#----------------R and Rstudio installation----------------------
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends wget r-cran-rgl libglu1-mesa-dev \
   libatlas3-base libx11-dev libcgal-dev libeigen3-dev g++ python-dev autotools-dev libicu-dev build-essential \
   libbz2-dev xvfb xauth xfonts-base xserver-xorg freeglut3 freeglut3-dev libgdal-dev libgeos++-dev libssl-dev

#----To simulate that we have X11-----
RUN apt-get update
RUN Xvfb :7 -screen 0 1280x1024x24 &
RUN export DISPLAY=:7

#-----R installation----
RUN echo "deb http://cloud.r-project.org/bin/linux/debian stretch-cran35/" >> /etc/apt/sources.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys FCAE2A0E115C3D8A
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y r-base r-base-dev
RUN echo 'options(repos = c(CRAN = "https://cran.rstudio.com"))' > ~/.Rprofile

#-----RStudio-----
RUN echo "deb http://ftp.de.debian.org/debian jessie main" >> /etc/apt/sources.list
RUN apt-get update && apt-get upgrade -y && apt-get install -y openssl libssl1.0.0

RUN wget https://download2.rstudio.org/rstudio-server-1.1.442-amd64.deb
RUN gdebi --n rstudio-server-1.1.442-amd64.deb
RUN rm -f rstudio-server-1.1.442-amd64.deb
RUN rstudio-server start

#RUN R --no-save -e 'install.packages(c("RJSONIO", "colourpicker", "snow", "threejs", "devtools", "cowplot", "Rcpp", "rjson", "Rvcg", "geometry", "Morpho", "data.table", "dbscan", "bnlearn", "msm", "Hmisc", "mclust"))'


WORKDIR /
COPY ./docker_web_entrypoint.sh ./docker_web_entrypoint.sh

CMD ["./docker_web_entrypoint.sh"]
