#!/bin/bash
echo "EMS_Clustering docker starts..."

docker-compose down --remove-orphans

echo "Deploying EMS_Clustering..."
docker-compose up --remove-orphans --build --force-recreate
echo "EMS_Clustering finished!"
